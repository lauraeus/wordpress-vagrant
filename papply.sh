#!/usr/bin/env bash
#
# This bootstraps Puppet on Ubuntu 12.04 LTS.
#
cd /etc/puppet
git checkout master
git pull origin master
puppet apply manifests/vagrant.lauraeus.se.pp --verbose
