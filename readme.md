### Lokal utveckling av Wordpress i VPS via Vagrant och virtualbox
  VirtualBox
  Vagrant 
  Puppet 
  Wordpress
  phpmyadmin

### Instruktioner 
    git clone https://lauraeus@bitbucket.org/lauraeus/wordpress-vagrant.git wordpress_local
    cd wordpress_local
    git submodule update --init
    git submodule foreach --recursive git checkout master
    vagrant up
    
    Wordpress URL:
    http://vagrant.lauraeus.se

#### Stänga av
    vagrant halt

#### Redigera wordpress
    cd wordpress
    arbeta som normalt med GIT

### Importera innehåll 
    Valfritt: redigera filen c:\Windows\System32\drivers\etc\hosts och addera
    192.168.33.17 www.wordpress.local 

    Kopiera bilder och content till wordpress/wp-content/uploads

    Importera en befintlig databas
    http://vagrant.lauraeus.se/phpmyadmin
    login: wordpress
    lösen: vagrant

    
### GIT Repository
    git submodule add https://lauraeus@bitbucket.org/lauraeus/wordpress.git wordpress
