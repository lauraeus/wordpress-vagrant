#Quick Manifest to stand up a demoserver mot bitbucket
Exec { path => [ "/bin/", "/sbin/" , "/usr/bin/", "/usr/sbin/" ] }

  host { 'vagrant.lauraeus.se':
    ensure       => 'present',
    host_aliases => ['vagrant'],
    ip           => '127.0.1.1',
    target       => '/etc/hosts',
  }

  ## Signallera 
  notify { "Fqdn är satt till ${fqdn}" : require => Host ['vagrant.lauraeus.se'], }

  package {'git':
    ensure  => latest,
    require => [File ['/root/.ssh/config'], File ['/root/.ssh/id_rsa'], File ['/root/.ssh/id_rsa.pub']],
  }

	file { '/root/.ssh':
		ensure => directory,
		owner => 'root',
		group => 'root',
	}

	file { '/root/.ssh/id_rsa':
		ensure => present,
		owner => 'root',
		group => 'root',
		mode  => '0600',
		source => '/vagrant/conf.d/id_rsa',
		require => File ['/root/.ssh'],
	}

	file { '/root/.ssh/id_rsa.pub':
		ensure => present,
		owner => 'root',
		group => 'root',
		source => '/vagrant/conf.d/id_rsa.pub',
		require => File ['/root/.ssh'],
	}

	file { '/root/.ssh/config':
		ensure => present,
		owner => 'root',
		group => 'root',
		mode  => '0600',
		source => '/vagrant/conf.d/config',
		require => File ['/root/.ssh'],
	}

  # Clear workspace
  exec { 'clear git workspace':
    cwd     => '/etc/puppet',
    command => 'rm /etc/puppet/puppet.conf',
    unless  => '/usr/bin/test -f /etc/puppet/.git/config',    
  }

	 # Init git repo
  exec { 'git init':
    cwd     => '/etc/puppet',
    command => 'git init',
    unless  => '/usr/bin/test -f /etc/puppet/.git',
    require => [Exec ['clear git workspace'], Package ['git'] ],
  }

  # Trap door to only allow add remote once
  file { '/etc/puppet/.git_remote_add_done':
    ensure  => present,
    content => "git remote add origin bitbucket ... completed",
    owner   => "root",
    group   => 'root',
    mode    => '0644',
  }

  # Setup git remote
  exec { 'setup git remote':
    cwd     => '/etc/puppet',
    command => 'git remote add origin https://lauraeus@bitbucket.org/lauraeus/puppet-vagrant-lemp.git',
    unless  => '/usr/bin/test -f /etc/puppet/.git_remote_add_done',
    before  => File['/etc/puppet/.git_remote_add_done'],
    require => Exec ['git init'],
  }
